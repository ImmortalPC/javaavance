package com.identicat;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;


/***************************************************************************//**
 * @brief Cette classe est chargée d'afficher les RSS téléchargé
 * @author Immortal-PC
 */
public class ViewRSS extends Activity implements OnClickListener
{
	private Button m_returnToMainPage = null;//!< Bouton de retour


	/***********************************************************************//**
	 * @brief Constructeur
	 * @param[in] savedInstanceState		Instance précédente
	 * @return[NONE]
	 */
	@SuppressLint("SetJavaScriptEnabled")
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rssactivity);

		// Ajout de l'icone HOME entant que btn de navigation
		getActionBar().setDisplayHomeAsUpEnabled(true);//@TargetApi(Build.VERSION_CODES.HONEYCOMB)

		// Bouton précédant
		m_returnToMainPage = (Button) findViewById(R.id.returnToMainPage);
		m_returnToMainPage.setOnClickListener(this);

		// WebView
		WebView bro = (WebView) findViewById(R.id.bro);

		// Si on a pas de données
		if( MainActivity.getJSON_data() == null || MainActivity.getJSON_data().equals("") ){
			bro.loadData("<div style='text-align:center;'>Pas de donn&eacute;es pour le momment...</div>", "text/html", null);
			return ;
		}

		bro.getSettings().setJavaScriptEnabled(true);


		// On laisse au JavaScript le soin de charger les données. Après tout, le JSON a été conçut pour lui.
		bro.loadData(
			"<script type=\"text/javascript\">"+
			"var data="+MainActivity.getJSON_data()+";\n"+
			"var pos = 0;\n"+
			"var lastUser = null;\n"+
			"for( var i=0; i<data['results'].length; i++ ){"+
			"	if( lastUser != data['results'][i]['profile_image_url'] ){"+
			"		if( lastUser != null )"+
			"			document.write('</div>');"+

			"		lastUser = data['results'][i]['profile_image_url'];"+

			"		document.write('<div style=\"min-height:50px; margin-bottom:15px; ');"+
			"		if( pos%2 ) {"+
			"			document.write('box-shadow:2px 2px 2px #aaa; border-bottom-right-radius:5px; text-align:right;\"><img src=\"'+data['results'][i]['profile_image_url']+'\" style=\"float:right;\" />'+data['results'][i]['text'].replace(new RegExp('([A-Za-z]*://[A-Za-z0-9\\.]+[^ \\r\\n\\t]*)','gim'), '<a href=\"$1\">$1</a>'));"+
			"		}else{"+
			"			document.write('box-shadow:-2px 2px 2px #aaa; border-bottom-left-radius:5px; \"><img src=\"'+data['results'][i]['profile_image_url']+'\" style=\"float:left;\" />'+data['results'][i]['text'].replace(new RegExp('([A-Za-z]*://[A-Za-z0-9\\.]+[^ \\r\\n\\t]*)','gim'), '<a href=\"$1\">$1</a>'));"+
			"		}"+
			"		pos++;"+
			"	}else{"+
			"		document.write('<br />'+data['results'][i]['text']);"+
			"	}"+
			"}"+
			"</script>", "text/html", null);

		/* Exemple avec la class JSONObject
			JSONObject json = new JSONObject(MainActivity.getJSON_data());

			JSONArray results = json.getJSONArray("results");
			JSONObject results_i = null;

			for( i=0; i<results.length(); i++ )
			{
				results_i = results.getJSONObject(i);
				results_i.getString("text");//<- text
				results_i.getString("profile_image_url");//<- text
			}


			JSONObject json = new JSONObject(t)
			json.getJSONArray("results").length()
		 */

		// On nettoie les notifications
		((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).cancelAll();
	}


	/***********************************************************************//**
	 * @brief Les event click sont géré ici
	 * @param[in] v		Element ayant provoqué l'even
	 * @return[NONE]
	 */
	@Override
	public void onClick( View v )
	{
		if( v == m_returnToMainPage ){
			if( !MainActivity.isOpened() )
				startActivity(new Intent(this, MainActivity.class));
			// On ferme l'activité
			this.finish();
		}
	}


	/***********************************************************************//**
	 * @brief Permet de gérer la touche retour
	 * @return[NONE]
	 */
	@Override
	public void onBackPressed()
	{
		if( !MainActivity.isOpened() )
			startActivity(new Intent(this, MainActivity.class));
		// On ferme l'activité
		this.finish();
	}


	/***********************************************************************//**
	 * @brief Permet de gérer l'icone HOME entant que btn de navigation
	 * @param[in] item	Element ayant provoqué l'event
	 * @return cf SDK
	 */
	@Override
	public boolean onOptionsItemSelected( MenuItem item )
	{
		if( item.getItemId() == android.R.id.home ){
			if( !MainActivity.isOpened() )
				startActivity(new Intent(this, MainActivity.class));
			// On ferme l'activité
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
