package com.identicat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;


/***************************************************************************//**
 * @brief Cette classe sert de service de téléchargement.
 * @author Immortal-PC
 */
public class IdentiCaService extends IntentService
{
	private boolean						m_continue = true;//!< Permet de break la boucle inf
	private static NotificationManager	m_notification = null;//!< Pointeur vers le system de notification
	private static PendingIntent		m_pendingIntent = null;//!< Activity a lancer lors d'un "click"

	/***********************************************************************//**
	 * @brief Constructeur
	 * @return[NONE]
	 */
	public IdentiCaService()
	{
		super("IdentiCaService");
		m_continue = true;
	}


	/***********************************************************************//**
	 * @brief Destructeur
	 * @return[NONE]
	 */
	@Override
	public void onDestroy()
	{
		m_continue = false;
		super.onDestroy();
	}


	/***********************************************************************//**
	 * @brief C'est dans cette fonction que l'on effectue le download.
	 * Cette fonction ce charge de faire les mise à jours tout les XXX temps.
	 * @param arg0	System appelant
	 * @return[NONE]
	 */
	@Override
	protected void onHandleIntent( Intent arg0 )
	{
		m_continue = true;
		while( m_continue )
		{
			String download = null;

			try{
				download = getFile("http://identi.ca/api/search.json?q="+URLEncoder.encode(MainActivity.getKeyword(),"US-ASCII"));

				if( download != MainActivity.getJSON_data() ){
					MainActivity.setJSON_data(download);
					showNotify();
				}


			}catch( IOException e ){
				// Erreur de lecture => On arrête le service
				stopServiceWithError("Erreur lors de la lecture du flux.\n"+e.getMessage());

			}catch( Exception e ){
				System.out.println("Exception non attendu !\n"+e.getMessage());
				e.printStackTrace();
			}

			try{
				Thread.sleep(MainActivity.getTimeOut());
			}catch( InterruptedException e ){
				e.printStackTrace();
			}
		}
	}


	/***********************************************************************//**
	 * @brief Permet de télécharger le JSON depuis une URL.
	 * @param url	L'url a télécharger
	 * @return Le JSON associé à l'url
	 * @throws IOException		En cas d'erreur vis à vis du http
	 */
	public String getFile( final String url ) throws IOException
	{
		HttpURLConnection huc = (HttpURLConnection) (new URL(url)).openConnection();

		if( huc.getResponseCode() == HttpURLConnection.HTTP_OK )
			throw new IOException("Erreur 404. URL introuvable.");

		BufferedReader input_br = new BufferedReader(new InputStreamReader(huc.getInputStream()));

		String line = null;
		String rep = "";

		// On lit tout
		while( (line = input_br.readLine()) != null )
			rep += line;

		input_br.close();

		return rep;
	}


	/***********************************************************************//**
	 * @brief Permet d'afficher une erreur puis d'avertir le service principale
	 * @param msg	Le message a afficher
	 * @return[NONE]
	 */
	private void stopServiceWithError( final String msg )
	{
		// On n'arrive pas a lire le JSON => On arrête le service
		m_continue = false;
		MainActivity.serviceStopped(msg);

		this.stopSelf();
	}


	/***********************************************************************//**
	 * @brief Permet d'envoyer des notification au System
	 * @return[NONE]
	 */
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private void showNotify()
	{
		if( m_notification == null )
			m_notification = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		if( m_pendingIntent == null )
			m_pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, ViewRSS.class), PendingIntent.FLAG_ONE_SHOT);

		int nbNews = 42;
		try{
			nbNews = (new JSONObject(MainActivity.getJSON_data())).getJSONArray("results").length();
		}catch(JSONException e){}// L'erreur importe peu...

		String msg = String.format(getResources().getString(R.string.NotifyDesc), nbNews);

		Notification notif = new Notification.Builder(this)
			.setWhen(System.currentTimeMillis())
			.setTicker(msg)
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentTitle(getResources().getString(R.string.NotifyTitle))
			.setContentText(msg)
			.setContentIntent(m_pendingIntent)
			.setAutoCancel(true)// On cache une fois que l'on a cliqué dessus
			.build();

		m_notification.notify(0, notif);
	}
}
