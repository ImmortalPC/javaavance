package com.identicat;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ToggleButton;


/***************************************************************************//**
 * @brief Class principale l'activité. C'est ici que l'on configure l'appli.
 * @author Immortal-PC
 */
public class MainActivity extends Activity implements OnClickListener, OnKeyListener
{
	private ToggleButton	m_tg = null;//!< Bouton d'activation/désactivation du service
	private Button			m_viewMsg = null;//!< Bouton pour voir les messages déjà download
	private EditText		m_urlFetch = null;//!< INPUT: Le mot clé a chercher
	private EditText		m_inputTimeout = null;//!< INPUT: timeout
	private Button			m_save = null;//!< Btn d'enregistrement des paramètres
	private Intent			m_service = null;//!< Le service a gérer
	private Intent			m_rssView = null;//!< Page contenant les rss

	private static MainActivity 	m_inst = null;//!< L'instance en cours

	// Donnée
	private static String	m_keyword = "minecraft";//!< Mot clé a chercher (default: cat)
	private static String	m_JSON_data = null;//!< Les données qui ont pu être download
	private static int		m_timeout = 15*60*1000;//!< Temps avant reDownload (15min)
	private static boolean	m_isServiceOn = false;//!< Permet de savoir l'etat du service
	private static String	m_lastError = null;//!< Contient la dernière erreur a donner à l'utilisateur

	// Création d'un fil d'échange avec e Service
	private static final Handler m_handler = new Handler() {
		@Override
		public void handleMessage( Message msg ){
			m_inst.m_tg.setChecked(false);
			m_inst.showLastError();
		}
	};


	/***********************************************************************//**
	 * @brief Main
	 * @param[in] savedInstanceState		Permet de restorer des paramètres enregistré
	 * @return[NONE]
	 */
	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainactivity);

		// Toogle Btn: Start/Stop service
		m_tg = (ToggleButton) findViewById(R.id.ServiceManager);
		m_tg.setChecked(m_isServiceOn);
		m_tg.setOnClickListener(this);

		// Bouton de visualisation
		m_viewMsg = (Button) findViewById(R.id.ViewActualMsg);
		m_viewMsg.setOnClickListener(this);

		// Input pour la recherche
		m_urlFetch = (EditText) findViewById(R.id.urlFetch);
		try{
			m_urlFetch.setHint(URLDecoder.decode(m_keyword,"US-ASCII"));
		}catch( UnsupportedEncodingException e ){
			m_urlFetch.setHint(m_keyword);
		}
		m_urlFetch.setOnKeyListener(this);

		// Timeout
		m_inputTimeout = (EditText) findViewById(R.id.inputTimeout);
		m_inputTimeout.setHint(Integer.toString(m_timeout/(1000*60)));
		m_inputTimeout.setOnKeyListener(this);

		// Bouton d'enregistrement
		m_save = (Button) findViewById(R.id.save);
		m_save.setOnClickListener(this);

		m_service = new Intent(MainActivity.this, IdentiCaService.class);
		m_rssView = new Intent(getApplicationContext(), ViewRSS.class);
		m_inst = this;

		showLastError();
	}


	/***********************************************************************//**
	 * @brief Destructeur du Main
	 * @return[NONE]
	 */
	@Override
	protected void onDestroy()
	{
		m_inst = null;
		super.onDestroy();
	}


	/***********************************************************************//**
	 * @brief Function pour l'event Click
	 * @param[in] arg0	L'element qui a déclenché l'event
	 * @return[NONE]
	 */
	@Override
	public void onClick( View arg0 )
	{
		// Activation / désactivation du service
		if( arg0 == m_tg ){
			if( m_tg.isChecked() ){
				m_isServiceOn = true;
				startService(m_service);
			}else{
				m_isServiceOn = false;
				stopService(m_service);
			}
			return ;
		}

		// Visualisation des anciens messages
		if( arg0 == m_viewMsg ){
			startActivity(m_rssView);
			return ;
		}

		// Sauvegarde des paramètres
		if( arg0 == m_save ){
			save(true, true);
			return ;
		}
	}


	/***********************************************************************//**
	 * @brief Function pour l'event keyDown
	 * @param[in] v			L'element qui a provoqué l'event
	 * @param[in] keycode	Le code de la touche/key
	 * @param[in] event		L'event originel
	 * @return FALSE => On laisse l'event passer
	 */
	@Override
	public boolean onKey( View v, int keycode, KeyEvent event )
	{
		m_save.setEnabled(true);

		// Mot clé
		if( v == m_urlFetch && keycode == 66 )// Touche Entrer
			save(true, false);

		// Timeout
		if( v == m_inputTimeout && keycode == 66 )
			save(false, true);

		return false;
	}


	/***********************************************************************//**
	 * @brief Permet d'enregistrer les param
	 * @param[in] saveKeyword	Enregistrer le mot clé ?
	 * @param[in] saveTimeout	Enregistrer le timeout ?
	 * @return[NONE]
	 */
	private void save( boolean saveKeyword, boolean saveTimeout )
	{
		m_save.requestFocus();
		m_save.setEnabled(false);

		if( saveKeyword ){
			try{
				m_keyword = URLEncoder.encode(m_urlFetch.getText().toString(),"US-ASCII");
			}catch( UnsupportedEncodingException e ){
				m_keyword = m_urlFetch.getText().toString();
			}
			m_urlFetch.setHint(m_keyword);
			m_urlFetch.setText("");
		}

		if( saveTimeout ){
			int tmp = 0;
			try{
				tmp = Integer.parseInt(m_inputTimeout.getText().toString());
			}catch( NumberFormatException e ){}

			if( tmp == 0 || tmp < 0 ){
				AlertDialog alertDialog = new AlertDialog.Builder(this).create();
				alertDialog.setTitle("IdentiCat");
				alertDialog.setMessage("Veuillez entrer une durée correct ! ( nombre entier supperieur à 0 )");
				alertDialog.show();
				return ;
			}

			if( m_timeout != tmp ){
				m_timeout = tmp*1000*60;
				m_inputTimeout.setHint(Integer.toString(tmp));
				m_inputTimeout.setText("");
			}
		}

		// On recharge le service
		if( m_isServiceOn ){
			stopService(m_service);
			startService(m_service);
		}
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir le mot clé a surveiller
	 * @return Le mot clé
	 * @note Appelée par le Service
	 */
	public static final String getKeyword()
	{
		return m_keyword;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir le timeout avant reDownload. (Temps en millisec)
	 * @return Le timeout > 0
	 * @note Appelée par le Service
	 */
	public static int getTimeOut()
	{
		return m_timeout;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir les données déjà download
	 * @return Les données déjà download
	 */
	public static final String getJSON_data()
	{
		return m_JSON_data;
	}


	/***********************************************************************//**
	 * @brief Permet de modifier les données déjà download
	 * @param[in] json  Les données déjà download
	 * @return[NONE]
	 * @note Les données sont protégée contre le xss
	 */
	public static void setJSON_data( final String json )
	{
		m_JSON_data = json.replace("<", "&lt;").replace(">", "&gt;");
	}


	/***********************************************************************//**
	 * @brief Cette fonction est appelée par le Service quand il vient a s'arrêter
	 * par lui même.
	 * @param[in] le	Une erreur rencontrer par le Service
	 * @return[NONE]
	 */
	public static void serviceStopped( final String le )
	{
		m_isServiceOn = false;
		m_lastError = le;
		if( m_inst == null )
			return ;
		m_inst.stopService(m_inst.m_service);

		// Mise sur la file des event
		m_handler.sendMessage(m_handler.obtainMessage());
	}


	/***********************************************************************//**
	 * @brief Permet d'afficher la dernière erreur rencontrée par le Service
	 * @return[NONE]
	 */
	private void showLastError()
	{
		if( m_lastError == null )
			return ;
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("IdentiCat");
		alertDialog.setMessage(m_lastError);
		alertDialog.show();
		m_lastError = null;
	}


	/***********************************************************************//**
	 * @brief Permet de savoir si la Main page est ouverte
	 * @return TRUE si la Main page est ouverte
	 */
	public static boolean isOpened()
	{
		return m_inst != null;
	}
}
