======
README
======

:Info: See <https://bitbucket.org/ImmortalPC/javaavance> for repo.
:Author: NUEL Guillaume (ImmortalPC)
:Date: $Date: 2012-12-16 $
:Revision: $Revision: 16 $
:Description: Application Android permettant de s'abonner à la surveillance d'un mot sur identi.ca


**Description**
---------------
Application Android permettant de s'abonner à la surveillance d'un mot sur identi.ca

| Ce programme comprend les fonctionnalité suivantes:

- Choix du mot à surveiller
- Choix de la durée entre 2 mise à jours
- Gestion des erreurs
- Utilisation du système de notification

| Attention !
| - Le programme ne garde que les 15 derniers messages !
| - Le programme ne garde pas les message en cas de reboot !


**Compilation**
---------------
- API min 16. (Nécéssaire pour Notification.Builder)
- SDK utilisé: adt-bundle-linux-x86


**Format** **du** **code**
--------------------------
- Le code est en UTF-8
- Le code est indenté avec des tabulations réel (\\t)
- La taille des tab conseillé est de 4
- Les fins de lignes sont de type LF (\\n)
- IDE utilisé: Eclipse


**Normes**
----------
- Les commentaires sont au format doxygen


**Documentation**
-----------------
Le code a entièrement documenté avec doxygen.
Pour générer la documentation:

>>> ./doxygen


**IDE** **RST**
---------------
RST créé grace au visualisateur: http://rst.ninjs.org/
